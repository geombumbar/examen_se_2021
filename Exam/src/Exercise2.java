public class Exercise2 extends Thread
{
            public Exercise2(String name)
            {
                super(name);
            }

            public static void main(String[] args)
            {
            Exercise2 AThread1 = new Exercise2("AThread1");
            Exercise2 AThread2 = new Exercise2("AThread2");

           AThread1.start();
            AThread2.start();

            }

            public void run()
            {
                for (int i = 0; i < 8; i++)
                {
                    System.out.println(getName() + " message number  = " + i);
                        try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e)
                       {
                                   e.printStackTrace();
                        }
            }
             System.out.println(getName() + " job finalised.");
            }
        }